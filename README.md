robin-hood
==========

## What is Multi-Armed Bandit? 

This is a self-optimizing split testing method detailed in this paper by Vermorel 
and Mohri: http://www.cs.nyu.edu/~mohri/pub/bandit.pdf. There is currently an implementation
in Ruby (https://github.com/bmuller/bandit), but I would like to expand and improve this
work.

### Goals

My goal with this project is to implement the epsilon-greedy method in various languages. I
plan to use this project to improve my Python (for shits), C (because its fast), and 
C# (because iPhone), so I am seeking pull requests for these implementations in particular. 
Ultimately, I will use this in production as a PHP library (don't scoff, I see you) and 
hopefully an npm module.